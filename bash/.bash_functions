#!/bin/bash

# Ancestor:
#   Recursively find the parent process of the current process, getting the
#   "Ancestor" process. Taken from: http://stackoverflow.com/questions/3586888
function ancestor {
    # Look up the parent of the given PID.
    if [ "$1" != "" ]; then
        pid=$1
    else
        pid=$$
    fi
    comm=$(ps -o comm= -p $pid 2>/dev/null) || return
    ppid=$(ps -o ppid= -p $pid 2>/dev/null) || return

    # /sbin/launchd always has a PID of 1, so if you reach that, the current PID is
    # the top-level parent. Otherwise, keep looking.
    if [[ ${ppid} -eq 1 ]] ; then
        echo "${comm}"
    else
        ancestor "${ppid}"
    fi
}

# Change Directory, Remove
#   Upon changing to a new directory with a '.DS_Store' file, remove it
function cdrm {
    [ -f "$1/.DS_Store" ] && rm "$1/.DS_Store";
    IFS=''
    builtin cd "$1"
    IFS=$'\n\t'
}

# Tickle
function tickle {
    deadline=$1
    shift
    task add +in +tickle wait:$deadline $@
}

# Path Munge:
#   Screw with the path, adding to front by default
function pathmunge {
    if [ -d "$1" ]; then
        if ! echo "$PATH" | grep -Eq "(^|:)$1($|:)" ; then
           if [ "$2" = "after" ] ; then
              PATH="$PATH:$1"
           else
              PATH="$1:$PATH"
           fi
        fi
    fi
}


# Botch:
#   Alternate form of watch(1)
#   Preserves unicode, color
botch() {
    # Get the desired interval from the user
    interval=0.5
    local OPTIND
    while getopts ":n:" opt; do
        case "$opt" in
            n)  interval=$OPTARG
                ;;
            \?) echo "Invalid option: -$OPTARG" >&2
                return
                ;;
            :)  echo "Option -$OPTARG requires an argument." >&2
                return
                ;;
        esac
    done

    # Confirm that it looks like an int or float
    case $interval in
        ''|*[!0-9\.]*) 
            echo "Invalid interval (1) '$interval'" >&2
            return
            ;;
        '.'|*.*.*)
            echo "Invalid interval (2) '$interval'" >&2
            return
            ;;
        *) 
            echo "Valid! '$interval'" >&2
            ;;
    esac

    # Reposition option index if needed
    shift $((OPTIND-1))

    # Execute the given command with the provided interval
    while true; do
        IFS=' '
        (printf '\033[H'
            CMD="$*"
            bash -c "$CMD" | while read LINE; do 
                echo -en '\033[2K'
                echo  "$LINE"
            done
            echo -en '\033[J') | sed '1!G;h;$!d' | sed '1!G;h;$!d'
        IFS=$'\n\t'
        sleep "$interval"
    done
}
