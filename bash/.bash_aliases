#!/bin/bash

# Shortened handles for key programs
alias n="nvim"
alias v="vim"
alias t="tmux -2"

# Tmux Aliases
alias tl="tmux ls"
alias ta="tmux -2 attach"

# Filesystem Navigation
alias ls="ls --color=auto --group-directories-first"
alias lsl="ls -lh --color=auto --group-directories-first"
alias lsa="ls -alh --color=auto --group-directories-first"
alias clr="clear && clear"
alias mkdir="mkdir --mode=755"
alias path='echo $PATH | tr : "\n"'
alias gitroot='cd $(git root)'
alias git_localtree="git grl \$(git merge-base --octopus \$(git branch --column | tr -d '*'))^.."
alias conflicts="n \$(git diff --name-only --diff-filter=U)"
alias pullall="for branch in \$(git for-each-ref --format='%(refname:short)' refs/heads/*); do git fetch -u origin \$branch:\$branch; done"

# Shortcuts to various scripts/functions
alias gtfo="rm -rf xvlog.* && rm -rf xsim.dir/ && rm -rf xvhdl.*"
alias not_asking='sudo $(history -p !!)'
alias swaps="ls ~/.local/share/nvim/swap/ ~/.vim/swapfiles | sed 's/%/\//g'"
alias log='nvim $(today).md'
alias cd="cdrm"
alias botanist="botch tree -FCl --dirsfirst" #Get it? Watching trees!
alias jnl='nvim $HOME/Documents/Journal/$(today).md'
alias pbvim="pbpaste | vimpipe | pbcopy"
alias tbvim="tmux showb | vimpipe | tmux loadb -"
alias pbj="pbpaste | ( jq '.' || pbpaste ) | pbcopy" #Haha, PB&J
alias pylint="pylint --disable superfluous-parens,\
invalid-name,\
too-many-nested-blocks,\
too-many-branches,\
too-many-statements,\
too-many-locals,\
too-many-lines,\
too-many-boolean-expressions,\
too-many-arguments,\
multiple-statements,\
fixme"
alias push='pushbullet push LE2115 note "$(date)"'
alias cgrep="compgen -c | grep "
alias today="date +\"%Y-%m-%d\""
alias datetime="date +\"%Y-%m-%d_%H:%M:%S\""
alias desaturate="sed -r \"s/[[:cntrl:]]\[[0-9]{1,3}m//g\""
alias blank="tput civis; clear; read -n 1; clear; tput cnorm"
alias sauce="file=\"\$(pick_sauce)\" && source \"\$file\""
alias tt="taskwarrior-tui"

alias in="task add +in"
alias tick="tickle"
alias think="tickle +1d"
