#!/bin/bash
# Set vi-mode readline
set -o vi

# CONFIGURATION
# Add color
export TERM="xterm-256color"
export CLICOLOR=1
export LC_COLLATE="C"

# Declare Default Editor
export EDITOR=vim

# Configure bash history
shopt -s histappend
export HISTSIZE=1000000
export HISTFILESIZE=1000000000
export HISTCONTROL=ignoreboth
export HISTTIMEFORMAT="[%R:%S on %a %b %d]: "


# ADD-INS
# Add git prompt
export PROMPT_COMMAND=":"
export GIT_PROMPT_ONLY_IN_REPO=1
export GIT_PROMPT_FETCH_REMOTE_STATUS=0
export GIT_PROMPT_THEME="Custom"
if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
    source "$HOME/.bash-git-prompt/gitprompt.sh"
fi

# Add bash tabcomplete
if [ -f /etc/bash_completion ]; then
    source /etc/bash_completion
fi

# Add ssh tabcomplete
if [ -f /etc/bash_completion.d/ssh ]; then
    source /etc/bash_completion.d/ssh
fi

# Add Fuzzy Finder
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Add PRLL
[ -f ~/Scripts/prll ] && source ~/Scripts/prll

# Add "most" as ideal pager tool
if command -v most > /dev/null 2>&1; then
    export PAGER=most
fi


# CONDITIONALS
if [[ "$OSTYPE" == "linux"* ]]; then
    export VISUAL=more
    echo 'exec "$@"' > "$HOME/Scripts/reattach-to-user-namespace"
    chmod +x "$HOME/Scripts/reattach-to-user-namespace"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    export VISUAL=less
elif [[ "$OSTYPE" == "win32" ]]; then
    :
else
    :
fi

if [[ ! -z "$TMUX" ]]; then
    if [[ "$(tmux display-message -p '#S')" == "0" ]]; then
        tmux rename-session "main"
    fi
fi

# SOURCE FILES
source "$HOME/.bash_functions"
source "$HOME/.bash_aliases"

# Path includes
pathmunge "$HOME/Bin" after
pathmunge "$HOME/Scripts" after
pathmunge "/opt/local/bin"
pathmunge "/opt/local/sbin"
pathmunge "/opt/local/libexec/gnubin/"
pathmunge "/usr/local/sbin" after
pathmunge "/usr/sbin" after
pathmunge "/sbin" after
pathmunge "/usr/local/bin" after
pathmunge "$HOME/Go" after
pathmunge "$HOME/.cargo/bin" after
pathmunge "$HOME/.pbsh"
pathmunge "$HOME/.android-sdk-macosx/platform-tools" after
pathmunge "$HOME/.nimble/nim/bin"
pathmunge "$HOME/.nimble/bin"
pathmunge "$HOME/bin" after

# Setup bash prompt
export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
if ps1 > /dev/null; then
    export PROMPT_COMMAND="$PROMPT_COMMAND; PS1X=\$(ps1);"
fi
export PROMPT_COMMAND="$PROMPT_COMMAND THISPC=\$( [ -f ~/.hostname ] && cat ~/.hostname || hostname -s)"
export PS1='\[\033[01;32m\]\u\[\033[0m\]@\[\033[01;96m\]${THISPC}\[\033[0m\]:${PS1X} \$ '

# DEVICE-SPECIFIC CONFIG
if [ -f "$HOME/.bash_specific" ]; then
    source "$HOME/.bash_specific"
fi
