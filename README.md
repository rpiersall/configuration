DOTFILES
========

Requirements
------------
[GNU Stow](https://www.gnu.org/software/stow/stow.html)

Installation:
-------------
```
stow bash git ssh tmux vim task other
```

Or just run
```
bash install.sh
```
