#!/usr/bin/env python2
"""
Explain:
    Gives a brief summary of the purpose and functionality of a script
    Iterates through the first few lines of a file, grabbing commented info
"""

# Import common modules
import os
import sys
import ast

# Constants
VALID_README_NAMES = ['README.md',
                      'README.txt',
                      'README']
COMMENT_PREFIXES = ['#', '/']
DOCSTRING_MARKERS = ['"""', "'''"]
MAX_LINES = 10

def main():
    """
    If target is a git repo, return the README.
    If it's a script, return top-of-file comments and docstrings.
    """
    # For each of the filenames supplied
    for argument in sys.argv[1:]:
        if os.path.isfile(argument):
            # If the item is a file, try to open it for reading
            try:
                script_info = get_script_info(argument)
                docstring = ast.get_docstring(ast.parse(''.join(open(argument))))
                docstring = '\n\t'.join(docstring.splitlines())
            except IOError:
                print "Could not read contents of '{}'\n".format(argument)
                continue
            except (SyntaxError, AttributeError):
                docstring = ""
            print "[1;32m{}[0m\n{}\t{}".format(argument, script_info, docstring)
            continue
        elif os.path.isdir(argument):
            # If the item is a folder, check if it's a git repo
            git_dir = os.path.join(argument, '.git')
            if os.path.isdir(git_dir):
                # If it is a git repo, check for a readme file
                try:
                    # If a valid readme exists, get and print its contents
                    readme_contents = get_readme(argument)
                    print "[1;34m{}[0m\n{}\n".format(argument, readme_contents)
                except IOError:
                    print "[1;34m{}[0m is a git repository. No valid README found\n"\
                            .format(argument)
                continue
            else:
                print "[1;34m{}[0m is not a top-level git repo\n".format(argument)
                continue
        else:
            print "[1;31m{}[0m is neither a file nor a directory\n".format(argument)
            continue



def get_script_info(script_path):
    """ Parse the contents of the script to get comments and docstrings """
    # Try to open the script
    script_file = open(script_path, 'r')
    script_lines = []
    while True:
        # Iterate through the file's lines
        script_line = script_file.readline()
        if script_line[:1] in COMMENT_PREFIXES:
            # If the line is a comment, grab it
            script_lines.append(script_line)
            continue
        else:
            break
    # Return the set of top-of-file-comment lines
    script_info = '\t' + '\t'.join(script_lines)
    return script_info



def get_readme(repo_path):
    """ Get the README file if it's a git repo """
    for readme_name in VALID_README_NAMES:
        try:
            # Check for any kind of README file
            readme_file = open(os.path.join(repo_path, readme_name), 'r')
        except IOError:
            continue
        else:
            # If the file was found and opened, get the first MAX_LINES lines
            contents = readme_file.read()
            content_lines = contents.splitlines()
            abridged_lines = content_lines[:MAX_LINES]
            abridged_contents = '\t' + '\n\t'.join(abridged_lines)
            return abridged_contents
    # If no README files were found, raise an IOError
    raise IOError

if __name__ == '__main__':
    main()
