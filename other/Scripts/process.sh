#!/bin/bash
# Process:
#   Iterate over taskwarrior "inbox" and process via a GTD workflow
#   Author: Richard Piersall <richard.piersall@gmail.com>
#   Last Change: YYYY-MM-DD

set -eo pipefail

function prompt {
    query=$1
    default=$2
    while true; do
        case $default in
            [Yy]* ) read -n 1 -rp "$query [Y/n]: " yn;;
            [Nn]* ) read -n 1 -rp "$query [y/N]: " yn;;
            *     ) read -n 1 -rp "$query [y/n]: " yn;;
        esac
        [ "$yn" != "" ] && echo "" >&2
        case $yn in
            [Yy]* ) echo 1; break;;
            [Nn]* ) echo 0; break;;
            * ) case $default in
                [Yy]* ) echo 1; break;;
                [Nn]* ) echo 0; break;;
                * ) echo "Please answer yes or no." >&2;;
            esac
        esac
    done
}




echo "Processing Taskwarrior inbox"
echo "There are $(task count +in status.not:completed status.not:waiting) items in your inbox"

index=0
for inbox_uuid in $(task _uuids +in status.not:completed status.not:waiting); do
    index=$((index+1))
    #echo "${inbox_uuid}"
    echo "Item ${index}: \"$(task _get "${inbox_uuid}.description")\""

    if [ "$(prompt "Actionable?" "y")" = 0 ]; then
        echo "    Okay, then what to do for now?"
        if [ "$(prompt "    Snooze?")" = 1 ]; then
            echo -n "        When? "
            read -r wait_time
            task modify "${inbox_uuid}" wait:"${wait_time}"
        elif [ "$(prompt "    Archive?")" = 1 ]; then
            task modify "${inbox_uuid}" -in +archive status:completed
        elif [ "$(prompt "    Delete?")" = 1 ]; then
            echo "        Bye bitch"
            task delete "${inbox_uuid}"
        else
            echo "Skipping"
        fi
        continue
    fi

    # Okay, so it is actionable? Let's figure out the scope
    if [ "$(prompt "Atomic task?" "y")" = 0 ]; then
        echo    "    Okay, then let's make a project for it"
        echo -n "        Project id:   "
        read -r new_project_id
        echo -n "        Project desc: "
        read -r new_project_desc

        if [ -z "${new_project_id}" ]; then
            echo "        Error: empty id string. Skipping"
        fi

        # Start up a new project
        task add +next pro:"${new_project_id}" "${new_project_desc}"

        # Also add as many tasks as you can possibly think of for this project. Then check off the task
        while true; do
            echo -n "        Subtask id:   "
            read -r new_subtask_id
            if [ -z "${new_subtask_id}" ]; then
                break
            fi
            echo -n "        Subtask desc: "
            read -r new_subtask_desc
            task add +next pro:"${new_project_id}.${new_subtask_id}" "${new_subtask_desc}"
        done

        if [ "$(prompt "    Satisfied?")" = 1 ]; then
            echo "    Archiving inbox item \"$(task _get "${inbox_uuid}.description")\""
            task modify "${inbox_uuid}" -in +archive status:completed
        else
            echo "    Please finish processing the ${new_project_id} project,"
            echo "    Then archive inbox item \"$(task _get "${inbox_uuid}.description")\""
        fi
        continue
    fi

    # So it's an atomic, actionable item. How long will it take?
    if [ "$(prompt "More than 2min?" "y")" = 0 ]; then
        echo "    Knock it out, chief"
        task start "${inbox_uuid}"

        # Wait for input or a user timeout
        for i in $(seq 120 -1 0); do
            echo -ne "\r    ${i}s     "
            read -n 1 -r -t 1 input
            [ -z "$input" ] || break
        done
        task stop "${inbox_uuid}"

        echo ""
        if [ "$(prompt "    Done?")" = 1 ]; then
            echo "        Great job"
            task modify "${inbox_uuid}" -in +archive status:completed
        else
            echo "        Good start, clearly it's more involved"
            # Handle incomplete
        fi
        continue
    fi

    # Can it be delegated?
    if [ "$(prompt "Do *you* have to do this?" "y")" = 0 ]; then
        echo "    Cool, someone else's problem"
        echo -n "        Who can do it?          "
        read -r delegation_name
        echo -n "        When's it needed?       "
        read -r delegation_deadline
        echo -n "        When should they start? "
        read -r delegation_wait
        task modify   "${inbox_uuid}" -in +waiting
        task annotate "${inbox_uuid}" "Follow up with ${delegation_name}" due:"${delegation_deadline}" wait:"${delegation_wait}"
        continue
    fi

    # Okay, let's categorize it and put it in the queue
    echo "So it's an actionable, atomic task that'll take longer than 2 minutes and only you can do it"

    task_description="$(task _get "${inbox_uuid}.description")"
    if [ "$(prompt "    Keep existing description?" "y")" = 0 ]; then
        echo    "    old description: \"${task_description}\""
        echo -n "    new description: "
        read -r task_description
    fi

    parent_project=""
    while true; do 
        echo -n "    What project does it fit under?   "
        read -r parent_project
        if [ "$parent_project" == "?" ]; then
            for project in $(task _projects); do
                echo "        $project"
            done
            continue
        fi
        break
    done

    echo -n "    When's it due?                    "
    read -r task_deadline

    task add project:"${parent_project}" "${task_description}" due:"${task_deadline}"

    if [ "$(prompt "    Satisfied?" "y")" = 1 ]; then
        echo "    Archiving inbox item \"$(task _get "${inbox_uuid}.description")\""
        task modify "${inbox_uuid}" -in +archive status:completed
    else
        echo "    Please finish registering the new task,"
        echo "    Then archive inbox item \"$(task _get "${inbox_uuid}.description")\""
    fi
done
