#!/bin/bash

set -eo pipefail

DEFAULT_BASE_DIR=~/Desktop

base_dir=$1
if [ -z $base_dir ]; then
    base_dir=$DEFAULT_BASE_DIR
fi

fronts_file=$(ls $base_dir/*front.pdf | head -n 1)
backs_file=$(ls $base_dir/*back.pdf | head -n 1)
base_file=$(printf "%s\n%s\n" "$fronts_file" "$backs_file" | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/')
echo "Collating base file: $base_file"

pdfseparate $fronts_file ${base_file}front-%d.pdf
pdfseparate $backs_file ${base_file}back-%d.pdf

ls $base_dir/*_front-* > /tmp/fronts.txt
ls $base_dir/*_back-* > /tmp/backs.txt

list=$(paste -d'\n' /tmp/fronts.txt /tmp/backs.txt)
echo "Concatenating:"
echo $list | tr ' ' '\n' | sed 's/^.*$/\t&/g'

echo -n "Please provide a document filename (no path or extension, please): "
read outfile_name
if [ -z $outfile_name ]; then
    outfile_name=${base_file}united.pdf
fi
outfile_path=$(dirname $base_file)/$outfile_name.pdf
echo "Into output file $outfile_path"
pdfunite $list $outfile_path

echo "Finished. Removing source files"
mv $fronts_file $backs_file $list ~/.Trash/

echo "Done."
