# Tmux settings

# Version-specific commands [grumble, grumble]
# See: https://github.com/tmux/tmux/blob/master/CHANGES
# Thanks to Tom Hale on https://stackoverflow.com/questions/35016458
run-shell "tmux setenv -g TMUX_VERSION $(tmux -V | cut -c 6-)"

if-shell -b '[ "$(echo "$TMUX_VERSION < 2.1" | bc)" = 1 ]' \
  "set -g mouse-select-pane on; set -g mode-mouse on; \
    set -g mouse-resize-pane on; set -g mouse-select-window on"

# In version 2.1 "mouse" replaced the previous 4 mouse options
if-shell -b '[ "$(echo "$TMUX_VERSION >= 2.1" | bc)" = 1 ]' \
  "set -g mouse on"

# UTF8 is autodetected in 2.2 onwards, but errors if explicitly set
if-shell -b '[ "$(echo "$TMUX_VERSION < 2.2" | bc)" = 1 ]' \
  "set -g utf8 on; set -g status-utf8 on; set -g mouse-utf8 on"

# bind-key syntax changed in 2.4
if-shell -b '[ "$(echo "$TMUX_VERSION < 2.4" | bc)" = 1 ]' \
  "bind-key -t vi-copy v   begin-selection; \
   bind-key -t vi-copy V   select-line; \
   bind-key -t vi-copy C-v rectangle-toggle; \
   bind-key -t vi-copy y   copy-pipe 'xclip -selection clipboard -in'"

# Newer versions
if-shell -b '[ "$(echo "$TMUX_VERSION >= 2.4" | bc)" = 1 ]' \
  "bind-key -T copy-mode-vi v   send -X begin-selection; \
   bind-key -T copy-mode-vi V   send -X select-line; \
   bind-key -T copy-mode-vi C-v send -X rectangle-toggle; \
   bind-key -T copy-mode-vi y   send -X copy-pipe-and-cancel 'xclip -selection clipboard -in'"

# TMUX 2.9 changes style operators
if-shell -b '[ "$(echo "$TMUX_VERSION < 2.9" | bc)" = 1 ]' \
  "setw -g window-status-format \" #I-#P #W \"; \
   setw -g window-status-bg colour245; \
   setw -g window-status-fg colour240; \
   setw -g window-status-current-format \" #I-#P #W \"; \
   setw -g window-status-current-bg colour0; \
   setw -g window-status-current-fg colour255"

if-shell -b "[ \"\$(echo \"\$TMUX_VERSION >= 2.9\" | sed 's/[A-z]//g' | bc)\" = 1 ]" \
  "setw -g window-status-format \" #I-#P #W \"; \
   setw -g window-status-style bg=colour245,fg=colour240; \
   setw -g window-status-current-format \" #I-#P #W \"; \
   setw -g window-status-current-style bg=colour0,fg=colour255"

# Change prefix to C-a
set-option -g prefix C-a

# Use 'a' to send a prefix to a nested session
bind-key a send-prefix
# Use C-a C-a to switch to the most recent window
bind-key C-a last-window

# Use C-a C-k to clear the screen
bind C-k send-keys -R \; clear-history

# Set XTerm key bindings
setw -g xterm-keys on

# Activate bell
set-option -g bell-action any

# Set colors
set-option -g default-terminal "screen-256color"
set-option -g update-environment 'DISPLAY SSH_ASKPASS SSH_AUTH_SOCK SSH_AGENT_PID SSH_CONNECTION WINDOWID XAUTHORITY DBUS_SESSION_BUS_ADDRESS'

# Enable pasteboard
set-option -g default-command "reattach-to-user-namespace bash"

# Use ctrl-c to copy to pasteboard if pbcopy is available and ctrl-v to paste
if-shell -b 'pbcopy > /dev/null' \
    "bind-key C-c run \"tmux save-buffer - | pbcopy\""
bind-key C-v run "tmux paste-buffer"

# Add single-key commands for splitting panes in same path
bind | split-window -h -c '#{pane_current_path}'  # Split panes horizontal
bind - split-window -v -c '#{pane_current_path}'  # Split panes vertically

bind F5 respawn-pane -k \; clear-history
set-option -g history-limit 100000

# Better handling for copy mode
unbind [
bind Escape copy-mode

# Set reload key to r
bind r source-file ~/.tmux.conf \; display 'Reloaded config'

# Make pane-syncing easier
bind S set synchronize-panes

# Count sessions start at 1
set -g base-index 1

# for vim
set -g @resurrect-strategy-vim 'session'

# for neovim
set -g @resurrect-strategy-nvim 'session'

# Set an escape time
set -sg escape-time 0

# Use vim bindings
setw -g mode-keys vi

# Turn off repeat timer
set-option -g repeat-time 0

# Remap window navigation to vim
unbind-key j
bind-key j select-pane -D
unbind-key k
bind-key k select-pane -U
unbind-key h
bind-key h select-pane -L
unbind-key l
bind-key l select-pane -R

# Make resizing easier
bind C-S-k resize-pane -U 15
bind C-S-j resize-pane -D 15
bind C-S-j resize-pane -L 25
bind C-S-l resize-pane -R 25

# Set the title bar
set -g set-titles on
set -g set-titles-string '#(whoami)@#h #S/#W'

# Set prefix-highlight colors
set -g @prefix_highlight_fg 'black'  # default is 'colour231'
set -g @prefix_highlight_bg 'white'  # default is 'colour04'

# Set status bar
set -g status-bg colour236
set -g status-fg white
set -g status-interval 1
set -g status-left-length 90
set -g status-right-length 90
set -g status-justify left
set -g status-left "#[fg=green,bright]#(whoami)#[fg=white]@#[fg=cyan]#([ -f ~/.hostname ] && cat ~/.hostname || hostname -s)#[fg=white,dim]  "
set -g status-right "#{prefix_highlight} Session: #[fg=green]#S#[fg=white] #(date -u +'[%%R:%%S UTC]') [%R:%S on %a %b %d]"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-logging'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'nhdaly/tmux-scroll-copy-mode'
set -g @plugin 'Morantron/tmux-fingers'

# Initialize TMUX plugin manager
if-shell "[ -f ~/.tmux/plugins/tpm/tpm ]" "run '~/.tmux/plugins/tpm/tpm'"
