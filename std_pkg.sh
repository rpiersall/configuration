#!/bin/bash
declare -A osInfo;
osInfo[/etc/redhat-release]=yum
osInfo[/etc/arch-release]=pacman
osInfo[/etc/gentoo-release]=emerge
osInfo[/etc/SuSE-release]=zypp
osInfo[/etc/debian_version]=apt
osInfo[/etc/alpine-release]=apk

package_manager=""
for f in "${!osInfo[@]}"
do
    if [[ -f $f ]];then
        echo Package manager: ${osInfo[$f]}
        package_manager=${osInfo[$f]}
    fi
done

#Common
################################################################################
package_list=""
package_list="${package_list} git"                  # Because Linus Torvalds is a...
package_list="${package_list} vim"                  # Vi iMproved
package_list="${package_list} neovim"               # Newer Vim
package_list="${package_list} tmux"                 # Terminal Multiplexer
package_list="${package_list} stow"                 # Stash files for later
package_list="${package_list} htop"                 # Human-readable Table of Processes
package_list="${package_list} bc"                   # Basic Calculator
package_list="${package_list} curl"                 # Client URL tool
package_list="${package_list} wget"                 # Web Get
package_list="${package_list} nmap"                 # Map netowrk ports
package_list="${package_list} lynx"                 # Command-line web browser
package_list="${package_list} zip"                  # Compress into zip files
package_list="${package_list} unzip"                # Extract from zip files
package_list="${package_list} colordiff"            # Visualize file diffs more easily
package_list="${package_list} sshfs"                # Secure SHell File System
package_list="${package_list} tree"                 # Visualize filesystem structure
package_list="${package_list} minicom"              # Command-line serial emulator
package_list="${package_list} shellcheck"           # Linter for bash
package_list="${package_list} tidy"                 # Formatter for HTML

# Per-Distro
################################################################################
if [ "$package_manager" == apt ]; then
    # Debian
    package_list="${package_list} exuberant-ctags"      # Index identifiers in C code
    package_list="${package_list} silversearcher-ag"    # Like `grep`, but faster
    echo "Using \`$package_manager\` to install: $package_list"
    # shellcheck disable=SC2086
    sudo "$package_manager" install $package_list
fi

if [ "$package_manager" == pacman ]; then
    # Arch
    package_list="${package_list} ctags"                # Index identifiers in C code
    package_list="${package_list} the_silver_searcher"  # Like `grep`, but faster
    echo "Using \`$package_manager\` to install: $package_list"
    # shellcheck disable=SC2086
    sudo "$package_manager" -S $package_list
fi
