" .vimrc - startup file for Vim

" Runtime path, ^P, and pathogen
set rtp+=~/.vim/
execute pathogen#infect()

" Set utf-8 encoding
scriptencoding utf-8
set encoding=utf-8

" Syntax highlighting
syntax on
if filereadable( expand("$HOME/.vim/colors/molokai.vim") )
    colorscheme molokai
    highlight GitGutterAdd    guifg=#009900 guibg=#232526 ctermfg=2 ctermbg=236
    highlight GitGutterChange guifg=#bbbb00 guibg=#232526 ctermfg=3 ctermbg=236
    highlight GitGutterDelete guifg=#ff2222 guibg=#232526 ctermfg=1 ctermbg=236
else
    colorscheme industry
    highlight GitGutterAdd    guifg=#009900 guibg=#000000 ctermfg=2 ctermbg=0
    highlight GitGutterChange guifg=#bbbb00 guibg=#000000 ctermfg=3 ctermbg=0
    highlight GitGutterDelete guifg=#ff2222 guibg=#000000 ctermfg=1 ctermbg=0
endif

" Plugin-specific settings
let g:tex_flavor = "tex"
let g:airline_powerline_fonts = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#whitespace#skip_indent_check_ft = {'markdown': ['trailing']}
let g:airline_theme='murmur'
let g:ackprg = 'ag --nogroup --nocolor --column'
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
let g:ale_linters = {'python': ['pylint'], 'cpp': ['g++'], 'c': ['gcc'], 'verilog': ['null'], 'systemverilog': ['null']}
let g:ale_python_pylint_options = '--extension-pkg-whitelist=cv2'
let g:vimwiki_list = [{'path': '~/Work/', 'path_html': '/tmp/WorkWikiRender'}]

" Remap leader
let mapleader=","                   " Leader is comma

" Core vim settings
set nocompatible
set nomodeline
set viminfo='1000,f1,\"500,:100,/100

" Interface configuration
set mouse=a                         " Enable mouse usage
set laststatus=2                    " Persistent status bar
set number                          " Show line numbers
set ruler                           " Display line number and column number
set hidden                          " Hides buffers instead of closing them, keeping unsaved changes
set showcmd                         " Show the last command entered in the bottom-right
set updatetime=250                  " Delay for updating plugins, such as showing git diffs
set backspace=indent,eol,start      " Make backspace delete over newlines, indents, and insert start

" Visual aids
set cursorline                      " Highlight the current line number
set showmatch                       " Show matching brace
set hlsearch                        " Highlight all search pattern matches
set visualbell                      " Use visual bell

" Tabbing behavior
set autoindent                      " Copy indentation from previous line
set smartindent                     " Automatically adds one level of indentation in certain cases
set smarttab                        " Attempts to align with the previous line
set tabstop=4                       " Number of columns in a tab
set shiftwidth=4                    " How far text is indented with the '<<' and '>>' commands
set expandtab                       " Insert spaces when the tab key is pressed

" Tabcompletion
set wildmode=longest,full

" Configure whitespace display
set list                            " Display whitespace
set listchars=eol:¬,tab:>-,trail:.,extends:>,precedes:<

" Tagfile location
set tags+=./.git/tags;

set directory=$HOME/.vim/swapfiles//

" Enable plugins
filetype plugin on

" User-defined functions
command! GH :call TimeLapse()
command! HEX :call IHexChecksum()
command! BN :w | :bn
command! W :w
command! SPACE :set list
command! NSPACE :set nolist
command! RL :so $MYVIMRC
command! TRAILING :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>
command! VALL :vert sba
command! AH :execute 'resize' line('$')
command! Focus :let g:limelight_bop = '^' | :let g:limelight_eop = '$' | :Limelight 0.9
command! Copyable :set nolist | :set nonumber

" File templates
autocmd BufNewFile *.log 0r ~/.vim/skeleton.log
autocmd BufNewFile *.sh 0r ~/.vim/skeleton.sh
autocmd BufNewFile *.py 0r ~/.vim/skeleton.py

" Reabbreviations
cnoreabbrev ag Ack
cnoreabbrev aG Ack
cnoreabbrev Ag Ack
cnoreabbrev AG Ack

" User-defined keybindings
nnoremap <F3> bver<Space>
nnoremap <F4> :TagbarToggle<CR>
nnoremap <F5> :UndotreeToggle<CR>
nnoremap <F7> :noh<CR><esc>
nnoremap <F8> :TRAILING<CR>
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
nmap <silent> <M-k> :GitGutterPrevHunk<CR>
nmap <silent> <M-j> :GitGutterNextHunk<CR>
vmap <Leader>fR c<C-O>:set ri<CR><C-R>"<Esc>:set nori<CR>

" Visual-mode search command
vnoremap // y/<C-R>"<CR>

" Automatic handling of binary files
augroup Binary
    au!
    au BufReadPre  *.bin,*.psd,*.elf,*.bit let &bin=1
    au BufReadPost *.bin,*.psd,*.elf,*.bit if &bin | %!xxd
    au BufReadPost *.bin,*.psd,*.elf,*.bit set ft=xxd | endif
    au BufWritePre *.bin,*.psd,*.elf,*.bit if &bin | %!xxd -r
    au BufWritePre *.bin,*.psd,*.elf,*.bit endif
    au BufWritePost *.bin,*.psd,*.elf,*.bit if &bin | %!xxd
    au BufWritePost *.bin,*.psd,*.elf,*.bit set nomod | endif
augroup END

" Set vim-commentary commentstring for various filetypes
autocmd FileType *      setlocal commentstring=//\ %s
autocmd FileType vim    setlocal commentstring=\"\ %s
autocmd FileType sh     setlocal commentstring=#\ %s
autocmd FileType tcl    setlocal commentstring=#\ %s
autocmd FileType python setlocal commentstring=#\ %s
autocmd FileType tex    setlocal commentstring=%\ %s
autocmd FileType cmake  setlocal commentstring=#\ %s
autocmd FileType conf   setlocal commentstring=#\ %s
autocmd FileType cfg    setlocal commentstring=#\ %s
autocmd FileType tmux   setlocal commentstring=#\ %s
autocmd FileType spice  setlocal commentstring=*\ %s
autocmd FileType make   setlocal commentstring=#\ %s

" Set filetype-specific tab behavior
autocmd FileType c      setlocal expandtab
autocmd FileType cpp    setlocal expandtab

" Neovim-only settins
if has("nvim")
    tnoremap <Esc> <C-\><C-n>
    autocmd TermOpen source ~/.bash_profile
endif
