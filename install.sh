#!/bin/bash

set -e

BACKUPS_FOLDER=".backups"
COMMON_DOTFILES="\
    .bashrc \
    "

# Check if `stow` is installed
if ! stow -V > /dev/null 2>&1; then
    echo "Please install GNU Stow first"
    exit 1
fi

# Create these folders if they don't exist yet
mkdir -pv ../.config/
mkdir -pv ../.ssh/

# Check for existing common files
echo "Installing dotfiles..."
for common_dotfile in $COMMON_DOTFILES; do
    # Check if it exists and isn't a symlink
    if [ -e "../$common_dotfile" -a ! -L "../$common_dotfile" ]; then
        echo "A \"$common_dotfile\" file already exists in $(readlink -f ../)"
        if [ -e ".backups/$common_dotfile" ]; then
            echo "and there's already a backup in .backups/"
            echo "please try again after you've moved one or the other"
            exit 1
        fi

        # Prompt user for input
        while true; do
            read -p "    Shall I store it here in .backups? [Y/n] " yn
            case $yn in
                "" | [Yy]* )
                    echo -ne "    "
                    mv -v "../$common_dotfile" ".backups/"
                    break
                    ;;
                [Nn]* )
                    echo "Then please try again after you've dealt with it"
                    exit 1
                    ;;
                * )
                    echo "Please answer yes or no."
                    ;;
            esac
        done
    fi
done

# Do the actual install
stow bash git ssh tmux vim task other

echo "done!"
